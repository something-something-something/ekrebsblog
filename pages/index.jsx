// import dynamic from 'next/dynamic';

// import Page from '../components/Page'
import Head from 'next/head'
import Link from 'next/link'
import {useRouter} from 'next/router';

import {SphereAnimCanvas} from '../components/3d/SphereAnimCanvas';

import {TorusKnotAnimCanvas} from '../components/3d/TorusKnotAnimCanvas';

import {PointShiftCanvas} from '../components/3d/PointShiftCanvas';

import { PointAnimCanvas} from '../components/3d/PointAnimCanvas';
// const DynamicTorusKnotAnimCanvas=dynamic(
// 	()=>{
// 			return import('../components/3d/TorusKnotAnimCanvas').then(
// 				(mod)=>{
// 					return mod.TorusKnotAnimCanvas
// 				}
// 			)
// 	}
// 	,{ssr:false}
// );

export default function HomePage(){
	const router=useRouter();
	let webglBackground=<PointAnimCanvas/>;
	switch (router.query.homePageBackground) {
		case 'torus':
			webglBackground=<TorusKnotAnimCanvas/>;
			break;
		case 'points':
			webglBackground=<PointShiftCanvas/>;
		break;
		case 'points2':
			webglBackground=<PointAnimCanvas/>;
		break;
		case 'orbits':
			webglBackground=<SphereAnimCanvas/>;
		break;
		default:
			webglBackground=<PointAnimCanvas/>;
		break;
	}
		return (
		<><Head><title>Eric Krebs</title></Head>
		<div style={{height:'100vh',width:'100vw',backgroundColor:'rgb(0,0,0)'}}>
		{webglBackground}
		

		{/* <TorusKnotAnimCanvas/> */}
		{/* todo move into component: */}
		<div style={{position:'fixed',bottom:'3rem',left:0,width:'100vw',fontFamily:'Blackout Sunrise',fontSize:'10vmin',justifyContent:'center',display:'flex'}}>
			<Link href="/blog">
				<a >Blog</a>
				
			</Link>
			<style jsx>{`
					a{
						color:rgb(0,0,0);
						color:transparent;
						font-weight:700;			
						text-decoration:none;
						text-shadow:0vmin 0vmin 1vmin rgb(0,0,0);
						background-clip:text;
						-webkit-background-clip:text;
						background-image:linear-gradient( 90deg,rgb(0,0,0) 33%,rgb(255,255,255), rgb(255,255,255) , rgb(0,0,0), rgb(0,0,0) 66%);
						 background-size:300% 100%;
						background-position:0% 0%;
						animation-name:BlogLinkAnimation;
						animation-duration:4s;
						
						animation-iteration-count:infinite;
						animation-timing-function:linear;
						transition-property:text-shadow;
						transition-duration:1s;
						transition-timing-function:linear;
						
					}
					a:hover{
						text-shadow:0vmin 0vmin 3vmin rgb(255,255,255);
					}
					@keyframes BlogLinkAnimation{
						0%{
							
							background-position:100% 0%;
						}
						80%{
							
							background-position:100% 0%;
						}
						100%{
							background-position:0% 0%;
						}
						
					}
				`}
				</style>
		</div>
		
	</div>
	</>);
}