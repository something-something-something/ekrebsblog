import '@fontsource/blackout-sunrise';
import '@fontsource/blackout-midnight';
import '@fontsource/blackout-two-am';
import '@fontsource/major-mono-display'
import '@fontsource/orbitron';
import '@fontsource/raleway/latin-ext.css';
import '@fontsource/raleway/latin.css';

import '@fontsource/megrim'
import '../style.css';
import {Transition,TransitionGroup,CSSTransition,SwitchTransition} from 'react-transition-group'

import {useRouter} from 'next/router'

function MyApp({Component, pageProps}){
	
	const router=useRouter();
	let num=10;
	let squarArr=[];
	for (let i=0;i<num; i++ ){
		for (let j=0;  j<num; j++){
			//0-60
			//180-250
			let hue=0;
			// if(i>j){
			// 	hue=130*((j+1)/(i+1));
			// }
			// else{
			// 	hue=130*((i+1)/(j+1));
			// }
			//hue=(130/num)*((j/2)+(i/2));
			if(i%2===0&&j%2===0){
				hue=Math.pow(i+12,j+45)%127
			}
			else if(i%2===0&&j%2===1){
				hue=Math.exp(i)%131;
			}
			else if(i%2===1&&j%2===1){
				hue=Math.exp(j)%127;
			}
			else{
				hue=Math.pow(j+13,i+78)%131;
			}
			
			if(hue>60){
				hue+=120
			}
			// hue=hue*2
			let sat=50;
			let light=50+(20/num)*i;
			squarArr.push(<div key ={'i:'+i+'j:'+j} className="theSquare"
			style={{
				top:(i*(100/num))+'vmax',
				left:(j*(100/num))+'vmax',
				width:(100/num)+'vmax',
				height:(100/num)+'vmax',
				transitionDelay:(100*(i+45)*(j+23))%97+'ms',
				backgroundColor:'hsl('+hue+','+sat+'%,'+light+'%)'
			}}
			></div>)
		}
	}
	

	return (
	<>
	<SwitchTransition mode="out-in">
		<CSSTransition timeout={600} key={router.asPath} classNames={{
			enter:'squareAnimEnter',
			enterActive:'squareAnimEnterActive',
			enterDone:'squareAnimEnterDone',
			exit:'squareAnimExit',
			exitActive:'squareAnimExitActive',
			exitDone:'squareAnimExitDone',
		}}>
			<div>
				{squarArr}

			
				
				<Component {...pageProps} />
			</div>
		</CSSTransition>
		
	</SwitchTransition>

		
	</>
	);
}
export default MyApp;