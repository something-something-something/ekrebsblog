// import fs from 'fs/promises';
// import path from 'path';

import {serialize} from 'next-mdx-remote/serialize';
import Page from '../../components/Page'
import {MDXRemote} from 'next-mdx-remote'
import {getBlogPosts} from '../../lib.jsx';
import Link from 'next/link'
import {BlogDate,HorizontalLine,CodeBlock,Heading,BlockQuote,Paragraph,BlogImage,RunExampleFunction} from '../../components/blog/main';

export default function PostPage(props){
	let postDate=new Date(props.data.date);
	let components={
		// code:(props)=>{
		// 	const codeTextRef=useRef();
		// 	return 	<div style={{backgroundColor:'rgb(20,20,20)',padding:'2rem',color:'rgb(0,250,150)'}}>
		// 		<button style={{backgroundColor:'rgb(0,250,150)',color:'rgb(20,20,20)',fontSize:'1rem',fontFamily:'Orbitron',padding:'0.5rem',border:'none'}} onClick={()=>{
		// 			navigator.clipboard.writeText(codeTextRef.current.innerText)
		// 		}}>Copy Code</button>
		// 		<pre ref={codeTextRef} >{props.children}</pre>
		// 	</div>
		// },
		code:CodeBlock,
		p:Paragraph,
		inlineCode:(props)=>{
			return <code style={{backgroundColor:'rgb(20,20,20)',borderRadius:'0.5rem',display:'inline-block', padding:'0.5rem',color:'rgb(0,250,150)',fontSize:'1rem'}}>{props.children}</code>
		},
		a:(props)=>{

			//console.log(props);
			try{
				let hrefLink=new URL(props.href);
				//console.log(hrefLink);
				if(hrefLink.protocol==='http:'||hrefLink.protocol==='https:'){
					return <a href={props.href} rel="noopener" target="_blank">{props.children}</a>;
				}
				else{
					return <a href={props.href}>{props.children}</a>;
				}

			}
			catch(err){
				//console.log(err)
			}

			return <Link href={props.href}><a >{props.children}</a></Link>;
		},
		h1:(props)=>{
			return <Heading level={1}>{props.children}</Heading>
		},
		h2:(props)=>{
			return <Heading level={2}>{props.children}</Heading>
		},
		h3:(props)=>{
			return <Heading level={3}>{props.children}</Heading>
		},
		h4:(props)=>{
			return <Heading level={4}>{props.children}</Heading>
		},
		h5:(props)=>{
			return <Heading level={5}>{props.children}</Heading>
		},
		h6:(props)=>{
			return <Heading level={6}>{props.children}</Heading>
		},
		blockquote:BlockQuote,
		hr:HorizontalLine,
		img:BlogImage,
		RunExampleFunction:RunExampleFunction,
	}
	
	
	return (<Page title={props.data.title}>
		{/* <h1 style={{textAlign:'center',fontFamily:'Blackout Midnight',margin:'2rem' }}>{props.data.title}</h1> */}
		<Heading level={1}>{props.data.title}</Heading>
		<BlogDate format="long" date={postDate}/>
		<MDXRemote {...props.mdx} components={components}/>
		
		{/* <div><Link href="/blog">Return To Blog</Link></div> */}
		</Page>)
}

export async function getStaticPaths(){

	let posts=await getBlogPosts();
	return{
		paths:posts.map((p)=>{
			return {params:{blogslug:p.data.slug}};
		}),
		fallback:false
	}
}




export async function getStaticProps(context){
	let posts=await getBlogPosts();

	let post=posts.find((p)=>{
		return p.data.slug===context.params.blogslug
	})
	//console.log(post);
	let mdxSource=await serialize(post.content,{scope:post.data})

	return {props:{
		mdx:mdxSource,
		data:post.data

	}}

}