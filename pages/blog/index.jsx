import {getBlogPosts} from '../../lib';
import Page from '../../components/Page'
import Link from 'next/link'
import {BlogPostList} from '../../components/BlogPostList'

export default function BlogList(props){
	return (<Page title="Blog of Eric Krebs"> 
		<BlogPostList posts={props.posts}/>

		{/* {props.posts.map((el)=>{

			let dateFormat=new Intl.DateTimeFormat('en-US',{
				timeZone:'America/Chicago',
				hourCycle:"h12",
				hour:'numeric',
				minute:'2-digit',
				dayPeriod:'long',
				era:'short',
				weekday:'short',
				year:'numeric',
				month:'long',
				day:'numeric',
		
				timeZoneName:"short"
			})
			return (<div key={el.data.slug}>
				<h2>
					<Link href={'/blog/'+el.data.slug}>
					{el.data.title}
					</Link>
				 </h2>
				{dateFormat.format(el.data.date)}
				</div> )
		})} */}
		
	</Page>)
}



export async function getStaticProps(){
	let posts=await getBlogPosts();
	posts.sort((a,b)=>{

		return b.data.date-a.data.date;


	})
	return {props:{
		posts:posts
	}}
}