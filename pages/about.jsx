import Page from '../components/Page'
import Link from 'next/link'

export default function AboutPage(){
	return <Page title="About Eric Krebs">
		<div style={{textAlign:'center'}}>
		<p>I am Eric Krebs</p>
		<p>See my <a href="https://i.ekrebs.com" target="_blank">pictures of insects</a>.</p>
		<p><a href="https://gitlab.com/something-something-something/ekrebsblog/" target="_blank">Code for this website.</a></p>
		<p>Other code I wrote can be found on <a href="https://github.com/something-something-something" target="_blank">Github</a></p>
		</div>
	</Page>
}