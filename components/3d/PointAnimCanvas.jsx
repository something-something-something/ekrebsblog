import {Canvas, useFrame} from '@react-three/fiber';

import { useMemo, useRef } from 'react';
import { BufferAttribute } from 'three';

function PointCubeGenorator(){

	const pointsRef=useRef();
	const geomRef=useRef();

	let pointsNum=10000;
	let initialStateVerts=new Float32Array(pointsNum*3);
	for(let i=0;i<pointsNum;i++){
		let radius=6*Math.pow(Math.random(),1/3);
		let angle=Math.random();
		let x=radius*Math.cos(angle*Math.PI*2);
		let y=radius*Math.sin(angle*Math.PI*2);
		let z=0;
		initialStateVerts[3*i]=x;
		initialStateVerts[3*i+1]=y;
		initialStateVerts[3*i+2]=z;
	}

	let circleStateVectors=new Float32Array(pointsNum*3);
	for(let i =0;i<pointsNum;i++){
		let angleA=Math.random();
		let angleOffset=Math.random();
		if(angleOffset<0.25){
			angleOffset=0;
		}
		else if(angleOffset<0.5){
			angleOffset=0.25;
		}
		else if(angleOffset<0.75){
			angleOffset=0.5;
		}
		else if(angleOffset<1){
			angleOffset=0.75;
		}
		//angleA=angleA+angleOffset
		let radius=6+2*Math.sin(angleA*Math.PI*2);
		let x=radius*Math.cos(angleA*Math.PI*2);
		let y=radius*Math.sin(angleA*Math.PI*2);
		let z=6*Math.cos((angleOffset)*Math.PI*2);
		circleStateVectors[3*i]=x;
		circleStateVectors[3*i+1]=y;
		circleStateVectors[3*i+2]=z;
	}
	let spiralStateVectors=new Float32Array(pointsNum*3);
	let spiralFunc=()=>{
		let angleA=Math.random()*20-10;
		let radius=0.3;
		let x=radius*angleA*Math.cos(angleA*Math.PI*2);
		let y=radius*angleA*Math.sin(angleA*Math.PI*2);
		let z=angleA;
		// spiralStateVectors[3*i]=x;
		// spiralStateVectors[3*i+1]=y;
		// spiralStateVectors[3*i+2]=z;
		return {x,y,z}
	}
	for(let i =0;i<pointsNum;i++){
		let results=spiralFunc();
		// let angleA=Math.random()*20-10;
		// //let opisite=Math.random()>0.5?1:-1;
		// //Math.cos(angleA*Math.PI*2)
		// let radius=0.3;
		// let x=radius*angleA*Math.cos(angleA*Math.PI*2);
		// let y=radius*angleA*Math.sin(angleA*Math.PI*2)
		// //let z=Math.tan(angleA*Math.PI*8);
		// let z=angleA
		spiralStateVectors[3*i]=results.x;
		spiralStateVectors[3*i+1]=results.y;
		spiralStateVectors[3*i+2]=results.z;
	}
	let cubeSpiralStateVectors=new Float32Array(pointsNum*3);
	let cubeFunc=()=>{
		let face = Math.floor(Math.random() * 6);
			let width = 6;
			let range = width / 2;

			let x = 0;
			let y = 0;
			let z = 0;

			if (face === 0) {

				x = Math.random() * width - range;
				y = Math.random() * width - range;
				z = range;
			}
			if (face === 1) {

				x = Math.random() * width - range;
				y = Math.random() * width - range;
				z = -1 * range;
			}
			if (face === 2) {
				x = Math.random() * width - range;
				y = range;
				z = Math.random() * width - range;
			}
			if (face === 3) {
				x = Math.random() * width - range;
				y = -1 * range;
				z = Math.random() * width - range;
			}
			if (face === 4) {
				x = range;
				y = Math.random() * width - range;
				z = Math.random() * width - range;
			}
			if (face === 5) {
				x = -1 * range;
				y = Math.random() * width - range;
				z = Math.random() * width - range;
			}
		return {x,y,z}
	}
	for(let i =0;i<pointsNum;i++){

		if(i/pointsNum>0.5){
			
			cubeSpiralStateVectors[3 * i] = spiralStateVectors[3*i];
			cubeSpiralStateVectors[3 * i + 1] =spiralStateVectors[3*i+1];
			cubeSpiralStateVectors[3 * i + 2] = spiralStateVectors[3*i+2];

		}
		else {
			let results=cubeFunc();

			cubeSpiralStateVectors[3 * i] = results.x;
			cubeSpiralStateVectors[3 * i + 1] = results.y;
			cubeSpiralStateVectors[3 * i + 2] = results.z;

		}

		

	}
	let cubeStateVectors=new Float32Array(pointsNum*3);
	for(let i =0;i<pointsNum;i++){
		if(i/pointsNum>0.5){
			let results=cubeFunc();
			cubeStateVectors[3 * i] = results.x;
			cubeStateVectors[3 * i + 1] = results.y;
			cubeStateVectors[3 * i + 2] = results.z;
			

		}
		else {
			cubeStateVectors[3 * i] = cubeSpiralStateVectors[3*i];
			cubeStateVectors[3 * i + 1] =cubeSpiralStateVectors[3*i+1];
			cubeStateVectors[3 * i + 2] = cubeSpiralStateVectors[3*i+2];

			

		}
	}
	let explodeStateVectors=new Float32Array(pointsNum*3);
	for(let i =0;i<pointsNum;i++){
		explodeStateVectors[3 * i] = Math.random()*100-50;
		explodeStateVectors[3 * i + 1] =Math.random()*100-50;
		explodeStateVectors[3 * i + 2] = Math.random()*100-50;
	}
	console.log('hi')
	const morphPos=useMemo(()=>{
		return [new BufferAttribute(circleStateVectors,3),new BufferAttribute(spiralStateVectors,3),new BufferAttribute(cubeSpiralStateVectors,3),new BufferAttribute(cubeStateVectors,3),new BufferAttribute(explodeStateVectors,3)];	
	});
	useFrame((state)=>{
		geomRef.current.morphAttributes.position=morphPos;
		pointsRef.current.updateMorphTargets();
		pointsRef.current.rotation.x+=0.01;
		pointsRef.current.rotation.y+=0.01;
		pointsRef.current.rotation.z+=0.01;
		let progress=(state.clock.elapsedTime%50/50)*6;
		if(progress<1){
			pointsRef.current.morphTargetInfluences[0]=progress;
			pointsRef.current.morphTargetInfluences[1]=0;
		}
		else if(progress>=1&&progress<2){
			pointsRef.current.morphTargetInfluences[0]=Math.max(1+(1-progress),0);
			pointsRef.current.morphTargetInfluences[1]=progress-1;
		}
		else if(progress>=2&&progress<3){
			//pointsRef.current.morphTargetInfluences[0]=0;
			pointsRef.current.morphTargetInfluences[1]=Math.max(1+(2-progress),0);
			pointsRef.current.morphTargetInfluences[2]=Math.min(progress-2,1);
			//pointsRef.current.morphTargetInfluences[3]=Math.min((progress-2)/2,1);
		}
		else if(progress>=3&&progress<4){
			//pointsRef.current.morphTargetInfluences[0]=0;
			//pointsRef.current.morphTargetInfluences[1]=Math.max(1+(2-progress),0);
			pointsRef.current.morphTargetInfluences[2]=Math.max(1+(3-progress),0);
			pointsRef.current.morphTargetInfluences[3]=Math.min(progress-3,1);
		}
		else if(progress>=4&&progress<5){
			//pointsRef.current.morphTargetInfluences[0]=0;
			//pointsRef.current.morphTargetInfluences[1]=Math.max(1+(2-progress),0);
			pointsRef.current.morphTargetInfluences[3]=Math.max(1+(4-progress),0);
			pointsRef.current.morphTargetInfluences[4]=Math.min(progress-4,1);
		}
		else{
			//pointsRef.current.morphTargetInfluences[0]=0;
			pointsRef.current.morphTargetInfluences[4]=Math.max(1+(5-progress),0);
			//pointsRef.current.morphTargetInfluences[0]=progress-4;
		}
		

	});
	return (<points ref={pointsRef}>

		<bufferGeometry ref={geomRef} >
			<bufferAttribute attachObject={['attributes', 'position']} count={pointsNum } array={initialStateVerts} itemSize={3}  />
		</bufferGeometry>
		<pointsMaterial color='rgb(0,0,0)' size={0.05} morphTargets={true}/>
	</points>);
}

export function PointAnimCanvas(){
	return(<Canvas camera={{position:[0,0,20]}}  onCreated={({gl})=>{gl.setClearColor('rgb(255,255,255)')}}>
		<PointCubeGenorator/>
	</Canvas>);
}