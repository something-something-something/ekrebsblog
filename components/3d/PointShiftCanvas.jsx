import {Canvas, useFrame} from '@react-three/fiber';

import { useMemo, useRef } from 'react';
import { BufferAttribute } from 'three';

function RandomPoints(){
	const pointsRef=useRef();
	const geomRef=useRef();
	let vertsNum=10000;
	let randVerts=new Float32Array(vertsNum*3);
	let randVertsMorph=new Float32Array(vertsNum*3);
	let  randVertsSphereMorph=new Float32Array(vertsNum*3);
	
	for(let i =0;i<vertsNum;i++){
		let x=Math.random()*10-5;
		let y=Math.random()*10-5;
		let z=Math.random()*10-5;
		randVerts[i*3]=x;
		randVerts[i*3+1]=y;
		randVerts[i*3+2]=z;
	}
	for(let k =0;k<vertsNum;k++){
		let x=Math.random()*20-10;
		let y=Math.random()*20-10;
		let z=Math.random()*20-10;
		randVertsMorph[k*3]=x;
		randVertsMorph[k*3+1]=y;
		randVertsMorph[k*3+2]=z;
	}
	for(let k =0;k<vertsNum;k++){
		let randRadiusPart=Math.random()
		let radius=6*Math.sqrt(randRadiusPart);
		let angle=Math.random();
		let angleB=Math.random();
		let x=radius*Math.sin(angle*Math.PI)*Math.cos(angleB*Math.PI*2);
		let y=radius*Math.sin(angle*Math.PI)*Math.sin(angleB*Math.PI*2);
		let z=radius*Math.cos(angle*Math.PI);
		randVertsSphereMorph[k*3]=x;
		randVertsSphereMorph[k*3+1]=y;
		randVertsSphereMorph[k*3+2]=z;
	}
	const morphPos=useMemo(()=>{
		return [new BufferAttribute(randVertsMorph,3),new BufferAttribute(randVertsSphereMorph,3)];
	},[])
	useFrame((state)=>{
		//console.log(geomRef.current )
		pointsRef.current.rotation.x+=0.01;
		pointsRef.current.rotation.y+=0.01;
		pointsRef.current.rotation.z+=0.01;
;
		geomRef.current.morphAttributes.position=morphPos;
		pointsRef.current.updateMorphTargets();
		let progress=(state.clock.elapsedTime%50/50)*2;
		progress=progress>1?-1*Math.pow((progress-1),4)+1:progress*progress;
		
		//progress=(Math.sin( ( progress-0.5 ) *Math.PI/2  ) +1)/2;
		//console.log(progress);
		pointsRef.current.morphTargetInfluences[1]=progress;
	})
	console.log(randVerts)
	return <points ref={pointsRef}>
		<bufferGeometry ref={geomRef} attach="geometry" >
			<bufferAttribute attachObject={['attributes', 'position']} count={randVerts.length / 3} array={randVerts} itemSize={3}  />

			{/* <bufferAttribute attachArray={['morphAttributes', 'position']} count={randVertsMorph.length / 3} array={randVertsMorph} itemSize={3} /> */}
		
			
				
				
		</bufferGeometry>
		{/* <meshPhongMaterial color='rgb(255,0,0)'/> */}
		{/* <meshNormalMaterial/> */}
		<pointsMaterial color='rgb(255,0,0)' size={0.05} morphTargets={true}/>
		
	</points>
}



export function PointShiftCanvas(){
	return (<Canvas  camera={{position:[0,0,10]}}  onCreated={({gl})=>{gl.setClearColor('rgb(55,0,255)')}}>
		{/* <ambientLight/>
		<pointLight position={[0,0,10]} /> */}
		<RandomPoints/>
	</Canvas>);
}