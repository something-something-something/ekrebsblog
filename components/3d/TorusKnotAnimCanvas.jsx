import {Canvas,useFrame} from '@react-three/fiber';

import { useRef } from 'react';

function TorusKnotAnim(props){

	const meshRef=useRef();

	

	useFrame(()=>{
		meshRef.current.rotation.x+=Math.random()/1000;
		meshRef.current.rotation.y+=Math.random()/1000;
		meshRef.current.rotation.z+=Math.random()/1000;
		//console.log(torusKnotRef.current.attributes )
	});
	return (<mesh {...props} ref={meshRef}>
				<torusKnotBufferGeometry args={[4,0.05,1300,100,13,19]}/>
				<meshToonMaterial color="rgb(0,200,0)" args={[]}/>
			</mesh>);
}

export function TorusKnotAnimCanvas(){
	return (<Canvas  camera={{position:[0,0,10]}}>
		
			<TorusKnotAnim/>
		
		
		
			
		
		<pointLight position={[10,0,0]}/>
	</Canvas>);
}