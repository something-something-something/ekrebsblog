import {Canvas,useFrame} from '@react-three/fiber';

import { useMemo, useRef } from 'react';
import { Color, Object3D} from 'three';

function SphereAnim(props){

	const colorRef=useRef();
	const groupRef=useRef();
	const materialRef=useRef()
	const bigSpherColor=useMemo(()=>{
		return new Color('hsl(0,50%,50%)');
	},[]);

	useFrame((state)=>{
		
		groupRef.current.rotation.x+=Math.random()/1000;
		groupRef.current.rotation.z+=Math.random()/1000;
		groupRef.current.rotation.y+=Math.random()/1000;
		//groupRef.current.rotation.x=0.5*Math.PI;
		//console.log(torusKnotRef.current.attributes )
		// let colors=['red','green','blue']
		let colState=state.clock.elapsedTime%25;
		//materialRef.current.setValues({color:colors[colState]})
		
		
			let colShiftDir=Math.random()>0.2?1:-1;
		bigSpherColor.offsetHSL(colShiftDir*Math.random()/100 ,0,0);
		materialRef.current.setValues({color:bigSpherColor})
		
		
		//console.log(state.clock.elapsedTime*100)
	});
	let sphereNum=300;

	return (
		<group ref={groupRef} {...props}>

	
			<mesh  >
				<sphereBufferGeometry args={[4,30,30]}/>
				<meshToonMaterial ref={materialRef} args={[]}/>
					
			</mesh>
			{/* {((sN)=>{
				let sArr=[]; 
				for(let i=0;i < sN;i++){
					sArr.push(<SmallSphere sNum={i} sMax={sN} rotationRadius={6+(i/2)} key={i}/>)
				}
				return sArr;
			})(sphereNum)} */}
			<SmallSquare  sMax={sphereNum} />
		</group>	
		);
}
function SmallSquare(props){
	const smallSphereRef=useRef();
	
	const randColor=useMemo(()=>{ 
		let colorArr=[];
		for(let i =0;i<props.sMax;i++){
			let hue=Math.random()*131;
		
			if(hue>60){
				hue+=120
			}
			// if(i===0){
			// 	hue=120;
			// }
			colorArr.push( new Color('hsl('+hue+','+100+'%,'+50+'%)'));
		}
		
		return colorArr;
		
	},[]);
	const objectForMatrix=useMemo(()=>{
		return new Object3D();
	},[])
	const colorForCopying=useMemo(()=>{
		return new Color();
	},[])
	useFrame((state)=>{
		let clockStatus=Math.floor(state.clock.elapsedTime*100)%1000;
		let turn=(clockStatus/1000);
		for(let i=0;i<smallSphereRef.current.count;i++){
			let innerOrbitTurnAddition=450*(( i )%201)/201
			
			
			let turnWithOfset=turn+(i/props.sMax);
			
			//let innerOrbitTurn=((Math.floor(state.clock.elapsedTime*100)%300)/300)+ 450*(( props.sNum +400)%363)/363 ;
			
			let innerOrbitTurn=((Math.floor(state.clock.elapsedTime*100)%700)/700)+  innerOrbitTurnAddition;
			let radius=(10)+2*Math.sin(2*Math.PI*innerOrbitTurn);
			// objectForMatrix.position.set(
			// 	radius*Math.cos(2*Math.PI* turnWithOfset ),
			// 	2*Math.cos(2*Math.PI* innerOrbitTurn),
			// 	radius*Math.sin(2*Math.PI* turnWithOfset));
			
			
			objectForMatrix.position.x=radius*Math.cos(2*Math.PI* turnWithOfset );
			objectForMatrix.position.z=radius*Math.sin(2*Math.PI* turnWithOfset);
			objectForMatrix.position.y=2*Math.cos(2*Math.PI* innerOrbitTurn);
			objectForMatrix.rotation.y= -Math.PI*2*turnWithOfset;
			objectForMatrix.updateMatrix();
			smallSphereRef.current.setMatrixAt(i,objectForMatrix.matrix);
			//console.log('hi')
			//smallSphereRef.current.setColorAt(i,colorForCopying.set('hsl('+randHue[i]+','+70+'%,'+50+'%)'));
			smallSphereRef.current.setColorAt(i,colorForCopying.set(randColor[i]));
		}
		smallSphereRef.current.instanceMatrix.needsUpdate=true;
		smallSphereRef.current.instanceColor.needsUpdate=true;
	})

	//let hue=(Math.pow(props.sNum+1)/Math.(props.sMax))*60) 
	console.log('hi')
	return (<instancedMesh ref={smallSphereRef} args={[null,null,props.sMax]}>
			<boxBufferGeometry args={[0.1,1,1,]}/>
			{/* color={'hsl('+randHue+','+50+'%,'+50+'%)'}  */}
			<meshToonMaterial args={[{}]}/>
				
			
			
			
		</instancedMesh>);
}
export function SphereAnimCanvas(){
	return (<Canvas  camera={{position:[0,0,20]}} >
		
			<SphereAnim />			
		
		<pointLight position={[10,0,0]}/>
	</Canvas>);
}