import Link from 'next/link'
import styles from './BlogPostList.module.css';
import {BlogDate} from './blog/main';

export function BlogPostList(props){
	let postsLinks=props.posts.map((el)=>{

		return (<div className={styles.blogListPostContainer} key={el.data.slug}>
			<h2 className={styles.blogListHeading}>
				<Link href={'/blog/'+el.data.slug}>
				<a className={styles.blogListHeadingLink}>{el.data.title}</a>
				</Link>
			 </h2>
			<BlogDate format="short" date={el.data.date}/>
			</div> )
	})
	return postsLinks;
}