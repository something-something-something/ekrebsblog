import styles from './main.module.css';
import  {createElement, useRef, useState } from 'react';
export function BlogDate(props){
	let dateFormat=new Intl.DateTimeFormat('en-US',{
		timeZone:'America/Chicago',
		hourCycle:"h12",
		hour:'numeric',
		minute:'2-digit',
		dayPeriod:'long',
		era:'short',
		weekday:'short',
		year:'numeric',
		month:'long',
		day:'numeric',

		timeZoneName:"short"
	});
	if(props.format==='long'){
		dateFormat=new Intl.DateTimeFormat('en-US',{
			timeZone:'America/Chicago',
			hourCycle:"h12",
			hour:'numeric',
			minute:'2-digit',
			dayPeriod:'long',
			era:"long",
			weekday:'long',
			year:'numeric',
			month:'long',
			day:'numeric',
	
			timeZoneName:"short"
		})
	}
	return <div className={styles.blogListPostDate}>{dateFormat.format(props.date)}</div>;
}
export function CodeBlock(props){
		const codeTextRef=useRef();
		const [showCopied,setShowCopied]=useState(false);
		let lines=props.children.split('\n');
		let digits=Math.log10(lines.length);
			if(digits<0){
				digits=0;
			}
		digits=Math.floor(digits)+1;
		let linesToHighlight=[]

		if(props.highlightLines!==undefined){
			linesToHighlight=props.highlightLines.split(',').map((lineNum)=>{
				if(lineNum.includes('-')){
					let range=lineNum.split('-').map((r)=>{
						return parseInt(r.trim(),10);
					});
					if(range.every((el)=>{
						return !Number.isNaN(el);
					})&&range.length===2){
						let arr=[];
						for(let i=range[0];i<=range[1];i++){
							arr.push(i);
						}
						return arr;
					}
				}
				return parseInt(lineNum.trim(),10);
			}).flat().filter((el)=>{
				return Number.isInteger(el);
			});
		}

		return 	<div style={{backgroundColor:'rgb(20,20,20)',padding:'2rem',color:'rgb(0,250,150)'}}>
			<div style={{display:'flex',flexDirection:'row',flexWrap:'wrap',justifyContent:'space-between'}}>
				<button className={styles.codeBlockButton} onClick={()=>{
				navigator.clipboard.writeText(codeTextRef.current.innerText).then(()=>{
					setShowCopied(true)
					setTimeout(()=>{
						setShowCopied(false)
					},1*1000);
					//alert('Text Copied!')
				});

				}}>Copy Code</button> 
				<button className={styles.codeBlockButton} onClick={()=>{
					window.open(URL.createObjectURL(new Blob([props.children],{type:'text/plain'})))
				}}>View Raw</button>
			</div>
			{showCopied&&<div className={styles.codeBlockButtonMessage}>Code Copied!</div>}
			<div className={styles.codeBlockContainer} ref={codeTextRef} >{lines.map((line,num,arr)=>{
				let classes=[styles.codeBlockCodeLine]
				if(linesToHighlight.includes(num+1)){
					classes.push(styles.codeBlockCodeLineHighlight)
				}
				return <pre key={num} className={classes.join(' ')} style={{'--lineNumberWidth':digits+'ch'}} >{line}</pre>
			})}</div>
			
		</div>
}

export function Heading(props){
	let level=1;
	if(props.level!==undefined&&Number.isInteger(props.level)&&props.level<7&&props.level>0){
		level=props.level;
	}
	

	let Element=(props)=>{
		return createElement('h'+level,props);
	};
	return <Element className={styles.blogHeading} style={{'--headingLevel':level}}>{props.children}</Element> 
}

export function BlockQuote(props){
	return <blockquote className={styles.blogBlockQuote}>{props.children}</blockquote>
}
//fontSize:(9/level)+'rem',

export function Paragraph(props){
	return <p className={styles.blogParagraph} >{props.children}</p>
}

export function HorizontalLine(props){
	return <hr className={styles.blogHorizontalLine}/>
}

export function BlogImage(props){
	const [imageBig,setImageBig] =useState(false);

	let imgClass=[styles.blogImage];
	if(imageBig){
		imgClass.push(styles.blogImageBig);
	}
	return <img onClick={()=>{setImageBig(!imageBig)}} className={imgClass.join(' ')} src={props.src} alt={props.alt}/>
}

export function RunExampleFunction(props){
	const [funcOutput,setFuncOutput]=useState('');
	
	return <div>
		<button onClick={()=>{
			setFuncOutput(props.exampleFunction(...props.exampleArgs));
		}}>Run Example Code</button>
		<pre>
			{'Arguments: '+JSON.stringify(props.exampleArgs)}
		</pre>
		<pre>
			{funcOutput.toString()}
		</pre>
	</div>
}