import Header from './Header'
import styles from './Page.module.css'
import Head from 'next/head'

export default function Page(props){
	return (<>
		<Head><title>{props.title}</title></Head>
		<Header/>
		<main className={styles.main}>
			{props.children}
		</main>
	</>);
}