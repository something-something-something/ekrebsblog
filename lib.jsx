import fs from 'fs/promises';
import matter from 'gray-matter';
import path from 'path';

import {DateTime} from 'luxon';

export async function getBlogPostData(fileName){
	let fileText=await fs.readFile(path.join(process.cwd(),'posts',fileName),{encoding:'utf8'}) 

	let file= matter(fileText);

	console.log(file)
	let slug;
	let title;
	let date;
	let draft
	if(file.data.slug===undefined){
		slug=path.basename(fileName,'.mdx')
	}
	else{
		slug=file.data.slug;
	}

	if(file.data.title===undefined){
		title=slug;
	}
	else{
		title=file.data.title;
	}
	console.log(file.data.date);
	if(file.data.draft===undefined){
		draft=false;
	}
	else{
		draft=true;
	}
	if(file.data.date===undefined){
		console.log('set a date')
		date=0;
	}
	else{

		console.log('DATE')
		console.log(file.data.date)
		let dateFormat='M-d-y h:mma';
		let dateFormatOptions={
			zone:'America/Chicago',
		};
		
		
		 //d=DateTime.fromMillis(0);
		// console.log(DateTime.fromFormatExplain(file.data.date,dateFormat,dateFormatOptions));
		 let d=DateTime.fromFormat(file.data.date,dateFormat,dateFormatOptions);
		if(!d.isValid){
			console.log(file.data.date+ ' is invalid');
			
			date=0;
		}
		else{
			date=d.toMillis();
		}
	}
	return {
		data:{
			slug:slug,
			title:title,
			date:date,
			draft:draft,
		},

		content:file.content
	};
}

export async function getBlogPosts(){
	let postFiles=await fs.readdir(path.join(process.cwd(),'posts'));
	console.log(postFiles);

	postFiles=postFiles.filter((fn)=>{
		return path.extname(fn)==='.mdx';
	});
	let PostsWithDrafts= await Promise.all(postFiles.map((fn)=>{
		return getBlogPostData(fn);

	}))
	return PostsWithDrafts.filter((post)=>{
		return !post.data.draft || process.env.NODE_ENV!=='production';
	});
}
